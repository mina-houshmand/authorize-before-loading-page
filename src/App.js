import React, { useState } from "react";
import Login from "./components/login";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Errorpage from "./components/Errorpage";
import PanelSetting from "./components/panelSetting";


function App() {
  const [token, setToken] = useState();
  if (!token) {
    return <Login setToken={setToken} />;
  }
  return (
    <div className="wrapper">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />}></Route>
          <Route path="/error" element={<Errorpage />}></Route>
          <Route path="/smspanel" element={<PanelSetting />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
