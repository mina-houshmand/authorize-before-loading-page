import React, { useState } from "react";
import "../style.css";
import PropTypes from "prop-types";

async function loginUser(credentials) {
  return fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
}

export default function Login({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password,
    });
    setToken(token);
  };

  return (
    <div className="entrance RtL">
      <div className="container">
      <form onSubmit={handleSubmit}>
        <div className="user">
          <h3 className="center">ورود</h3>
          <div className="text">شناسه عبور</div>
          <input
            className="outline"
            placeholder="شناسه عبور"
            name="username"
            type="text"
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="password">
          <div className="text">رمز عبور</div>
          <input
            className="outline"
            placeholder="رمز عبور"
            name="password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="but">
          <button className="button" type="submit">
            ثبت
          </button>
        </div>
        </form>
      </div>
    </div>
  );
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired,
};
