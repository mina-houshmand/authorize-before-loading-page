import React, { useState } from "react";
import "../style.css";

function PanelSetting() {
  const [smsstate, setsmsstate] = useState({
    user: "",
    pass: "",
    apiKey: "",
  });
  const handleChange = (e, name) => {
    setsmsstate({
      ...smsstate,
      [name]: e.target.value,
    });
  };
  const handelclick = () => {
    console.log("you've done");
  };

  return (
    <div className="entrance RtL">
      <div className="container">
        <div className="user">
          <h3 className="center">تنظیمات پنل</h3>
          <div className="text">نام کاربری</div>
          <input
            className="outline"
            placeholder="نام کاربری"
            name="user"
            type="text"
            onChange={(e) => handleChange(e, "user")}
          />
        </div>
        <div className="password">
          <div className="text">رمز عبور</div>
          <input
            className="outline"
            placeholder="رمز عبور"
            name="user"
            type="password"
            onChange={(e) => handleChange(e, "pass")}
          />
        </div>
        <div className="password">
          <div className="text">API Key</div>
          <input
            className="outline"
            placeholder="API Key"
            name="user"
            type="text"
            onChange={(e) => handleChange(e, "apiKey")}
          />
        </div>
        <div className="but">
          <button className="button" type="submit" onClick={handelclick}>
            ثبت
          </button>
        </div>
      </div>
    </div>
  );
}

export default PanelSetting;
